import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<Person> persons = new ArrayList<>();
		persons.add(new Person.Builder()
				.name("Karel")
				.age(15)
				.address(new Address.Builder()
						.city("Pardubice")
						.postCode(53002)
						.build())
				.build());
		persons.add(new Person.Builder()
				.name("Zden�k")
				.age(55)
				.address(new Address.Builder()
						.city("Hradec Kr�lov�")
						.postCode(00003)
						.build())
				.build());
		persons.add(new Person.Builder()
				.name("Adam")
				.age(1)
				.address(new Address.Builder()
						.city("Praha")
						.postCode(56002)
						.build())
				.build());
		
		// Natural ordering
		//Collections.sort(persons);
		
		// Total ordering
		//Collections.sort(persons, Person.comparatorByCityAddress);
		
		Collections.sort(persons, new Comparator<Person>() {
			@Override
			public int compare(Person person1, Person person2) {
				return person1.compareTo(person2);
			}
		});
		
		// Lambda function
		Collections.sort(persons, (person1, person2) ->  {
			return person1.compareTo(person2);

		});
		
		persons.forEach(person -> System.out.println(person.toString()));
		
	}
}
