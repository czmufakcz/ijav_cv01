import java.util.Comparator;


public class Address {
	private String city;
	private int postCode;

	public Address() {

	}

	public Address(String city, int postCode) {
		super();
		this.city = city;
		this.postCode = postCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPostCode() {
		return postCode;
	}

	public void setPostCode(int postCode) {
		this.postCode = postCode;
	}

	@Override
	public String toString() {
		return "Address [city=" + city + ", postCode=" + postCode + "]";
	}

	public final static Comparator<Address> comparatorByCity = new Comparator<Address>() {

		@Override
		public int compare(Address address1, Address address2) {
			return address1.city.compareTo(address2.city);
		}
	};
	
	public static class Builder {
		private String city;
		private int postCode;
		
		public Builder city(String city) {
			this.city = city;
			return this;
		}
		
		public Builder postCode(int postCode) {
			this.postCode = postCode;
			return this;
		}
		
		public Address build() {
			Address address = new Address();
			address.setCity(city);
			address.setPostCode(postCode);
			return address;
		}
	}

}
